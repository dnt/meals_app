import React from 'react';
import {
  StyleSheet,
  FlatList,
} from 'react-native';

import {HeaderButtons, Item} from 'react-navigation-header-buttons';

import HeaderButton from '../components/HeaderButton';
import {CATEGORIES} from '../data/dummy-data';
import CategoryGridTile from '../components/CategoryGridTile';

const CategoriesScreen = props => {

    console.log(props);

    const renderGridItem = (itemData)=>{
      return (
      <CategoryGridTile 
        title={itemData.item.title} 
        color = {itemData.item.color}
        onSelect={() =>{
          props.navigation.navigate({
            routeName: 'CategoryMeals',     
              params:{
                categoryId: itemData.item.id
              }
          });
      }}
    />);
    };

    return(
      <FlatList
        keyExtractor={(item, index) => item.id}
        data={CATEGORIES}
        renderItem={renderGridItem}
        numColumns={2}
      />
    );
};

CategoriesScreen.navigationOptions = (navData)=> {
  return {
    //headerTitle:''
    headerTitle:'Meal Categories',
    headerLeft:()=>
      (<HeaderButtons HeaderButtonComponent={HeaderButton}>
        <Item 
          title="Menu" 
          iconName='ios-menu' 
          onPress={()=>{
            navData.navigation.toggleDrawer();
          }}
        />
      </HeaderButtons>)
    // headerStyle:{
    //   backgroundColor:Platform.OS === 'android' ? Colors.primaryColor: ''
    // },
    // headerTintColor: Platform.OS === 'android'? 'white' : Colors.primaryColor
  }
};

const styles=StyleSheet.create({
    screen:{
        flex:1,
        justifyContent: 'center',
        alignItems: 'center',
    }
});

export default CategoriesScreen;

/*
Object { --> Es el objeto props y su estructura
  "navigation": Object {  --> obj navegacion es una props y es además un objeto
    "actions": Object {   --> obj actions tipo props 
      "dismiss": [Function dismiss],
      "goBack": [Function goBack],
      "navigate": [Function navigate],
      "pop": [Function pop],
      "popToTop": [Function popToTop],
      "push": [Function push],
      "replace": [Function replace],
      "reset": [Function reset],
      "setParams": [Function setParams],
    },
    "addListener": [Function addListener],
    "dangerouslyGetParent": [Function anonymous],
    "dismiss": [Function anonymous],
    "dispatch": [Function anonymous],
    "emit": [Function emit],
    "getChildNavigation": [Function getChildNavigation],
    "getParam": [Function anonymous],
    "getScreenProps": [Function anonymous],
    "goBack": [Function anonymous],
    "isFirstRouteInParent": [Function isFirstRouteInParent],
    "isFocused": [Function isFocused],
    "navigate": [Function anonymous],
    "pop": [Function anonymous],
    "popToTop": [Function anonymous],
    "push": [Function anonymous],
    "replace": [Function anonymous],
    "reset": [Function anonymous],
    "router": undefined,
    "setParams": [Function anonymous],
    "state": Object {
      "key": "id-1592096140011-0",
      "routeName": "Categories",
    },
  },
  "screenProps": undefined,
}
*/